package fr.pe.client.task.emission.repository;

import software.amazon.awssdk.services.s3.model.DeleteObjectResponse;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

/**
 * repository de manipulation des objets bucket s3
 */
public interface S3ObjectRepository {

    /**
     * lister les buckets
     * @return List<String>
     */
    List<String> listBucket();

    /**
     * lister tous les fichiers d'un bucket
     * @return
     */
    List<String> listFile();

    /**
     * charger un fichier dans un bucket s3
     * @param file
     * @return
     * @throws IOException
     */
    PutObjectResponse upload(File file) throws IOException;

    /**
     * supprimer un fichier dans un bucket s3
     * @param fileName
     * @return
     */
    DeleteObjectResponse deleteFileFromS3Bucket(String fileName);

    /**
     * télécharger un fichier present dans un bucket dans les resources de l'apli
     * @param fileName
     * @param destination
     * @return
     */
    Boolean downloadFile(final String fileName, final Path destination);
}