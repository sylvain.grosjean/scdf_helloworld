package fr.pe.client.task.emission.service;

import fr.pe.client.task.emission.repository.S3ObjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.s3.model.DeleteObjectResponse;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
@Slf4j
public class S3ObjectServiceImpl implements S3ObjectService {

    public S3ObjectRepository s3ObjectRepository;

    @Value("${amazon.s3.bucket-name}")
    public String bucketName;

    @Value("${amazon.s3.end-point-url}")
    public String endpointUrl;

    @Autowired
    public S3ObjectServiceImpl(S3ObjectRepository s3ObjectRepository) {
        this.s3ObjectRepository = s3ObjectRepository;
    }

    @Override
    public List<String> listBucket() {
        return s3ObjectRepository.listBucket();
    }

    @Override
    public List<String> listFile() {
        return s3ObjectRepository.listFile();
    }

    @Override
    public PutObjectResponse upload(File file) throws IOException {
        return s3ObjectRepository.upload(file);
    }

    @Override
    public DeleteObjectResponse deleteFileFromS3Bucket(String fileName) {
        return s3ObjectRepository.deleteFileFromS3Bucket(fileName);
    }

    @Override
    public boolean downloadFile(String fileName, Path destination) throws IOException {
        Files.createDirectories(Paths.get(destination.toString()));
        return s3ObjectRepository.downloadFile(fileName, destination);
    }
}