package fr.pe.client.task.emission.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.http.apache.ApacheHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

import java.net.URI;

/**
 * classe de configuration du client Amazon Web Service
 */
@Configuration
@Slf4j
public class AWSConfiguration {

    @Value("${amazon.s3.end-point-url}")
    public String endpointUrl;

    @Value("${amazon.access-key}")
    public String accessKey;

    @Value("${amazon.secret-key}")
    public String secretKey;

    @Bean
    public AwsCredentialsProvider awsCredentialsProvider() {
        final AwsBasicCredentials credentials = AwsBasicCredentials.create(this.accessKey, this.secretKey);
        return StaticCredentialsProvider.create(credentials);
    }

    @Bean
    public S3Client amazonS3Client(AwsCredentialsProvider awsCredentialsProvider) {

        URI endpointOverride = URI.create(endpointUrl);

        return S3Client.builder()
                .httpClientBuilder(ApacheHttpClient.builder())
                .region(Region.EU_WEST_1)
                .endpointOverride(endpointOverride)
                .credentialsProvider(awsCredentialsProvider)
                .build();
    }

}
