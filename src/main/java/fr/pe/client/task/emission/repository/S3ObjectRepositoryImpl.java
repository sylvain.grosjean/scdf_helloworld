package fr.pe.client.task.emission.repository;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class S3ObjectRepositoryImpl implements S3ObjectRepository {

    Logger log = LoggerFactory.getLogger(Logger.class);

    private S3Client s3Client;

    @Value("${amazon.s3.bucket-name}")
    public String bucketName;

    @Autowired
    public S3ObjectRepositoryImpl(S3Client s3Client) {
        this.s3Client = s3Client;
    }

    @PostConstruct
    private void initializeAmazon() {

        log.info("vérifier que le bucket {} qu'on s'apprête à utiliser existe déjà sinon le créer avant la creation " +
                "du bean S3ObjectRepository", bucketName);
        ListBucketsResponse listBucketsResponse = s3Client.listBuckets();
        List<String> list = new ArrayList<>();

        log.info("liste des buckets : ");
        for (Bucket b : listBucketsResponse.buckets()) {
            log.info("* {}", b.name());
            list.add(b.name());
        }

        if (!list.contains(bucketName)) {
            log.info("Bucket inexistant : Création d'un nouveau Bucket {}", bucketName);
            s3Client.createBucket(CreateBucketRequest
                    .builder()
                    .bucket(bucketName)
                    .createBucketConfiguration(
                            CreateBucketConfiguration
                                    .builder()
                                    .locationConstraint(Region.EU_WEST_1.toString())
                                    .build())
                    .build());

        } else {
            log.info("Le Bucket existe déjà ! ({}})", bucketName);
        }
    }

    @Override
    public List<String> listBucket() {
        log.info("lister tous les buckets s3");
        ListBucketsResponse listBucketsResponse = s3Client.listBuckets();
        List<String> list = new ArrayList<>();

        log.info("Buckets:");
        for (Bucket b : listBucketsResponse.buckets()) {
            log.info("* {}", b);
            list.add(b.toString());
        }

        return list;
    }

    @Override
    public List<String> listFile() {
        log.info("lister tous les fichiers présent dans le bucket s3");
        ListObjectsRequest listObjects = ListObjectsRequest
                .builder()
                .bucket(bucketName)
                .build();

        ListObjectsResponse res = s3Client.listObjects(listObjects);
        List<S3Object> s3Objects = res.contents();

        //extraire une liste de string représentant un objet S3Object
        List<String> listFile = s3Objects
                .stream()
                .map(object -> Objects.toString(object, null))
                .collect(Collectors.toList());

        return listFile;
    }

    @Override
    public PutObjectResponse upload(File file) throws IOException {
        log.info("charger le fichier {} dans le bucket s3 {}", file.getName(), bucketName);
        log.info("" + s3Client);
        return s3Client.putObject(
                PutObjectRequest
                        .builder()
                        .bucket(bucketName)
                        .key(file.getName())
                        .build(),
                file.toPath());
    }

    @Override
    public DeleteObjectResponse deleteFileFromS3Bucket(String fileName) {

        log.info("supprimer le fichier présent dans le bucket s3");
        return s3Client.deleteObject(
                DeleteObjectRequest
                        .builder()
                        .bucket(bucketName)
                        .key(fileName)
                        .build());
    }

    @Override
    public Boolean downloadFile(String fileName, Path destination) {
        log.info("télécharger le fichier {} présent dans le bucket S3 vers {}", fileName, destination);
        try {
            if (Files.exists(destination)) {
                log.info("le fichier {} existe déjà on le supprime avant le téléchargement", fileName);
                Files.delete(destination);
            }
        } catch (final IOException e) {
            log.error("could not delete temp snapshotfile {}", destination.toString(), e);
            return false;
        }
        final GetObjectRequest request = GetObjectRequest
                .builder()
                .bucket(bucketName)
                .key(fileName)
                .build();

        final GetObjectResponse getObjectResponse = s3Client.getObject(request, destination);

        log.debug("download {} from bucket {}: ", fileName, bucketName, getObjectResponse.toString());

        return Boolean.TRUE;
    }

}