package fr.pe.client.task.emission.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Message {

    @JsonProperty("url_object_bucket")
    private String urlObjectBucket;

    @JsonProperty("id_client")
    private String idClient;

    @JsonProperty("nom_client")
    private String nomClient;

    @JsonProperty("nom_flux")
    private String nomFlux;

    @JsonProperty("key")
    private String key;


}