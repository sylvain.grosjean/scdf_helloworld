package fr.pe.client.task.emission.domain;

public class BatchModel {

    private String nomJob;
    private String urlOriginale;
    private String urlUtilisee;
    private String identifiantEnvironnement;
    private String identifiantCorrelation;
    private String nomApplicationAppelante;
    private String identifiantUtilisateur;
    private String codeChaine;
    private String dateTraitement;
    private String cheminFichierRapport;
    private String cheminFicherCodeRetour;
    private String nombreMinutesAttenteRapport;
    private String tempsMaximumExecutionBatch;
    private String proprietesAdditionnelles;

    public String getNomJob() {
        return nomJob;
    }

    public void setNomJob(String nomJob) {
        this.nomJob = nomJob;
    }

    public String getUrlOriginale() {
        return urlOriginale;
    }

    public void setUrlOriginale(String urlOriginale) {
        this.urlOriginale = urlOriginale;
    }

    public String getUrlUtilisee() {
        return urlUtilisee;
    }

    public void setUrlUtilisee(String urlUtilisee) {
        this.urlUtilisee = urlUtilisee;
    }

    public String getIdentifiantEnvironnement() {
        return identifiantEnvironnement;
    }

    public void setIdentifiantEnvironnement(String identifiantEnvironnement) {
        this.identifiantEnvironnement = identifiantEnvironnement;
    }

    public String getIdentifiantCorrelation() {
        return identifiantCorrelation;
    }

    public void setIdentifiantCorrelation(String identifiantCorrelation) {
        this.identifiantCorrelation = identifiantCorrelation;
    }

    public String getNomApplicationAppelante() {
        return nomApplicationAppelante;
    }

    public void setNomApplicationAppelante(String nomApplicationAppelante) {
        this.nomApplicationAppelante = nomApplicationAppelante;
    }

    public String getIdentifiantUtilisateur() {
        return identifiantUtilisateur;
    }

    public void setIdentifiantUtilisateur(String identifiantUtilisateur) {
        this.identifiantUtilisateur = identifiantUtilisateur;
    }

    public String getCodeChaine() {
        return codeChaine;
    }

    public void setCodeChaine(String codeChaine) {
        this.codeChaine = codeChaine;
    }

    public String getDateTraitement() {
        return dateTraitement;
    }

    public void setDateTraitement(String dateTraitement) {
        this.dateTraitement = dateTraitement;
    }

    public String getCheminFichierRapport() {
        return cheminFichierRapport;
    }

    public void setCheminFichierRapport(String cheminFichierRapport) {
        this.cheminFichierRapport = cheminFichierRapport;
    }

    public String getCheminFicherCodeRetour() {
        return cheminFicherCodeRetour;
    }

    public void setCheminFicherCodeRetour(String cheminFicherCodeRetour) {
        this.cheminFicherCodeRetour = cheminFicherCodeRetour;
    }

    public String getNombreMinutesAttenteRapport() {
        return nombreMinutesAttenteRapport;
    }

    public void setNombreMinutesAttenteRapport(String nombreMinutesAttenteRapport) {
        this.nombreMinutesAttenteRapport = nombreMinutesAttenteRapport;
    }

    public String getTempsMaximumExecutionBatch() {
        return tempsMaximumExecutionBatch;
    }

    public void setTempsMaximumExecutionBatch(String tempsMaximumExecutionBatch) {
        this.tempsMaximumExecutionBatch = tempsMaximumExecutionBatch;
    }

    public String getProprietesAdditionnelles() {
        return proprietesAdditionnelles;
    }

    public void setProprietesAdditionnelles(String proprietesAdditionnelles) {
        this.proprietesAdditionnelles = proprietesAdditionnelles;
    }

}
