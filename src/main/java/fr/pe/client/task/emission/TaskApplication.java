package fr.pe.client.task.emission;

import fr.pe.client.task.emission.domain.BatchModel;
import fr.pe.client.task.emission.domain.Message;
import fr.pe.client.task.emission.repository.S3ObjectRepository;
import fr.pe.client.task.emission.utils.FileUtils;
import org.springframework.kafka.core.KafkaTemplate;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.task.configuration.EnableTask;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Slf4j
@SpringBootApplication
@EnableTask
@EnableBatchProcessing
@Configuration
public class TaskApplication {

    @Autowired
    private S3ObjectRepository s3ObjectService;

    public static void main(String[] args) {
        SpringApplication.run(TaskApplication.class, args);
    }

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    private KafkaTemplate<String, Message> kafkaTemplate;

//    @Value("https://coronavirusapi-france.now.sh/FranceLiveGlobalData")
//    String covidUrl;

    @Value("${urlUtilisee}")
    String urlUtilisee;

    @Value("${nombreMinutesAttenteRapport}")
    Integer m_nbRetentatives;

    @Value("${amazon.s3.bucket-name}")
    private String bucketName;

    @Value("${amazon.s3.end-point-url}")
    public String endpointUrl;

    @Value("${app.directory.in}")
    public String dirIn;

    @Value("${app.multipartFile}")
    public String multipartFile;

    @Value("${app.idClient}")
    public String idClient;

    @Value("${app.topicName}")
    public String topicName;

//    @Bean
//    @Order(1)
//    public Job job1() {
//        return jobBuilderFactory.get("job1")
//                .start(stepBuilderFactory.get("job1step1")
//                        .tasklet((contribution, chunkContext) -> {
//                            URI uri = new URI(covidUrl);
//
//                            log.info("appel job1step1: {}", covidUrl);
//                            System.out.println("appel job1step1: " + covidUrl);
//
//                            RestTemplate restTemplate = new RestTemplate();
//
//                            ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
//
//                            log.info("reponse job1step1: " + response);
//
//                            return RepeatStatus.FINISHED;
//                        })
//                        .build())
//                .build();
//    }

    @Bean
    @Order(1)
    public Job lancementBatch() {
        return jobBuilderFactory.get("lancementBatch")
                .start(stepBuilderFactory.get("appelBA027")
                        .tasklet((contribution, chunkContext) -> {

                            log.info("appel appelBA027: " + urlUtilisee + "/execution");
                            RestTemplate restTemplate = new RestTemplate();
                            HttpHeaders headers = new HttpHeaders();
                            headers.setContentType(MediaType.APPLICATION_JSON);

                            BatchModel batchModelrequest = new BatchModel();
                            batchModelrequest.setNomJob("");
                            batchModelrequest.setUrlOriginale("");
                            batchModelrequest.setUrlUtilisee("");
                            batchModelrequest.setIdentifiantEnvironnement("");
                            batchModelrequest.setIdentifiantCorrelation("");
                            batchModelrequest.setNomApplicationAppelante("");
                            batchModelrequest.setIdentifiantUtilisateur("");
                            batchModelrequest.setCodeChaine("");
                            batchModelrequest.setDateTraitement("");
                            batchModelrequest.setCheminFichierRapport("");
                            batchModelrequest.setCheminFicherCodeRetour("");
                            batchModelrequest.setNombreMinutesAttenteRapport("");
                            batchModelrequest.setTempsMaximumExecutionBatch("");
                            batchModelrequest.setProprietesAdditionnelles("");

                            HttpEntity<BatchModel> requestEntity = new HttpEntity<BatchModel>(batchModelrequest, headers);

                            String urlInvocationLanceur = urlUtilisee + "/batch/lanceur/execution";
                            ResponseEntity<String> responseLancementBatch = restTemplate.exchange(urlInvocationLanceur, HttpMethod.POST, requestEntity, String.class);

                            log.info("reponse appelBA027: " + responseLancementBatch);

                            if (responseLancementBatch.getStatusCode().equals(HttpStatus.OK)) {
                                String idJob = responseLancementBatch.getBody();

                                ResponseEntity<String> responseDemandeRapport = invoquerRecuperationRapport(idJob, urlUtilisee);

                                log.info("Fin traitement Batch - Rapport : {}" + responseDemandeRapport);
                            }
                            return RepeatStatus.FINISHED;
                        })
                        .build())
                .build();
    }

    @Bean
    @Order(2)
    public Job emissionFichier() {
        return jobBuilderFactory.get("emissionFichier")
                .start(stepBuilderFactory.get("s3ETkafka")
                        .tasklet((contribution, chunkContext) -> {

                            Path path = Paths.get(dirIn + "/" + multipartFile );
                            log.info("chemin fichier: {}", path);

                            File file = new File(String.valueOf(path));

                            PutObjectResponse putObjectResponse = this.s3ObjectService.upload(file);

                            log.info("upload terminé: {}", file);

                            if (putObjectResponse.sdkHttpResponse().isSuccessful()) {
                                log.info("Notification Kafka");
                                String url = endpointUrl + "/" +bucketName + "/" + multipartFile;

                                Message messageKafka = new Message();
                                messageKafka.setIdClient(idClient);
                                messageKafka.setUrlObjectBucket(url);

                                kafkaTemplate.send(topicName, messageKafka);
                            }
                            return RepeatStatus.FINISHED;
                        })
                        .build())
                .build();
    }

    private ResponseEntity<String> invoquerRecuperationRapport(String p_idJob, String p_urlUtilisee) throws Exception {
        boolean erreurDeconnexion = true;
        boolean traitementOk = false;

        String urlInvocationRapport = urlUtilisee + "/batch/lanceur/rapport/" + p_idJob;
        RestTemplate restTemplate = new RestTemplate();


        ResponseEntity<String> responseDemandeRapport = null;
        while (this.m_nbRetentatives >= 0 && (true == erreurDeconnexion || false == traitementOk)) {
            erreurDeconnexion = false;
            traitementOk = false;
            try {
                log.info("Rdu rapport - {} tentatives restantes", Integer.valueOf(this.m_nbRetentatives));
                responseDemandeRapport = restTemplate.exchange(urlInvocationRapport, HttpMethod.GET, null, String.class);
                if (responseDemandeRapport.getStatusCode().equals(HttpStatus.OK)) {
                    traitementOk = true;
                } else {
                    erreurDeconnexion = true;
                    log.warn("Erreur la rdu rapport (code HTTP : {})", Integer.valueOf(responseDemandeRapport.getStatusCodeValue()));
                }
                log.debug("Code HTTP recuperation rapport : {}", Integer.valueOf(responseDemandeRapport.getStatusCodeValue()));
            } catch (Exception exc) {
                log.warn("Erreur la rdu rapport");
                log.debug("Erreur la rdu rapport", (Throwable) exc);
                if (null != exc.getCause() && exc.getCause() instanceof java.net.SocketException)
                    erreurDeconnexion = true;
                traitementOk = false;
            }
            gererAttenteSurErreur(erreurDeconnexion);
        }
        gererExceptionTraitement(erreurDeconnexion, traitementOk);
        return responseDemandeRapport;
    }

    private void gererExceptionTraitement(boolean p_erreurDeconnexion, boolean p_traitementOk) throws Exception {
        if (p_erreurDeconnexion) {
            log.error("Echec de connexion au domaine batch");
            throw new Exception();
        }
        if (false == p_traitementOk)
            throw new Exception();
    }

    private void gererAttenteSurErreur(boolean p_erreurDeconnexion) {
        if (p_erreurDeconnexion) {
            this.m_nbRetentatives--;
            if (this.m_nbRetentatives >= 0)
                try {
                    log.debug("Attente de {}s avant retry -  {} tentatives restantes", Integer.valueOf(this.m_nbRetentatives), Integer.valueOf(this.m_nbRetentatives));
                    Thread.sleep(this.m_nbRetentatives * 1000L);
                } catch (InterruptedException interruptedException) {
                }
        }
    }

}
