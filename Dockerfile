FROM maven/pe-maven-3.6.3-jdk-11-slim:0.0.6 AS builder

WORKDIR /app
COPY pom.xml .
COPY src ./src

RUN mvn -f ./pom.xml -e -B dependency:resolve dependency:resolve-plugins

RUN mvn clean package
RUN ls

FROM openjdk:11-jre-slim
COPY --from=builder  /app/target/*.jar app.jar
RUN sh -c 'touch /app.jar'
CMD [ "sh", "-c", "java -jar /app.jar" ]

