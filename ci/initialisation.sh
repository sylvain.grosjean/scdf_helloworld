#!/bin/sh
REPERTOIRE_PROJET=$(dirname "$0")

# Le nom du projet doit correspondre à celui défini dans le fichier <pipeine.yml>
# au niveau du Job de déploiement du pipeline (jobs > name > plan > put > params > pipelines > name)
NOM_PIPELINE="batch-emission"

# Ici <drosd-tmj> correspond à l'équipe déclarée dans le projet http://git-scm.pole-emploi.intra/plateforme/k8s/build/equipes
EQUIPE=drosd-tmj

fly -t $EQUIPE status

if [ $? -ne 0 ]; then
    fly -t $EQUIPE login -c http://concourse.pole-emploi.intra --team-name $EQUIPE
fi

fly -t $EQUIPE set-pipeline -c "$REPERTOIRE_PROJET/pipeline.yaml" -p "${NOM_PIPELINE}" -n
fly -t $EQUIPE unpause-pipeline -p "$NOM_PIPELINE"
